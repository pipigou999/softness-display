export default {
    'zh-CN': {
        i18n: {
            content: '你好，很高兴认识你！'
        }
    },
    'en-US': {
        i18n: {
            content: 'Hello, nice to meet you!'
        }
    }
}