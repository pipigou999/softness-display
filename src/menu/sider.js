import dashboard from './modules/dashboard';
import system from './modules/system';
import demo1 from './modules/demo1';
import demo2 from './modules/demo2';
import log from './modules/log';

export default [
    dashboard,
    system,
    demo1,
    demo2,
    log
];
