const pre = '/demo1/';

export default {
    path: '/demo1',
    title: '模块1',
    header: 'home',
    custom: 'i-icon-demo i-icon-demo-form',
    children: [
        {
            path: `${pre}func1`,
            title: '功能1'
        }, {
            path: `${pre}func2`,
            title: '功能2'
        }, {
            path: `${pre}func3`,
            title: '功能3'
        }, {
            path: `${pre}func4`,
            title: '功能4'
        }
    ]
}
