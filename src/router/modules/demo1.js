import BasicLayout from '@/layouts/basic-layout';

const meta = {
    auth: true
};

const pre = 'demo1-';

export default {
    path: '/demo1',
    name: 'demo1',
    redirect: {
        name: `${pre}func1`
    },
    meta,
    component: BasicLayout,
    children: [
        {
            path: 'func1',
            name: `${pre}func1`,
            meta: {
                ...meta,
                title: '功能1'
            },
            component: () => import('@/pages/demo1/fun1')
        }, {
            path: 'func2',
            name: `${pre}func2`,
            meta: {
                ...meta,
                title: '功能2'
            },
            component: () => import('@/pages/demo1/fun2')
        }, {
            path: 'func3',
            name: `${pre}func3`,
            meta: {
                ...meta,
                title: '功能3'
            },
            component: () => import('@/pages/demo1/fun3')
        }, {
            path: 'func4',
            name: `${pre}func4`,
            meta: {
                ...meta,
                title: '功能4'
            },
            component: () => import('@/pages/demo1/fun4')
        }
    ]
};
