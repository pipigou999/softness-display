import BasicLayout from '@/layouts/basic-layout';

const meta = {
    auth: true
};

const pre = 'demo2-';

export default {
    path: '/demo2',
    name: 'demo2',
    redirect: {
        name: `${pre}func1`
    },
    meta,
    component: BasicLayout,
    children: [
        {
            path: 'func1',
            name: `${pre}func1`,
            meta: {
                ...meta,
                title: '功能1'
            },
            component: () => import('@/pages/demo2/fun1')
        }, {
            path: 'func2',
            name: `${pre}func2`,
            meta: {
                ...meta,
                title: '功能2'
            },
            component: () => import('@/pages/demo2/fun2')
        }, {
            path: 'func3',
            name: `${pre}func3`,
            meta: {
                ...meta,
                title: '功能3'
            },
            component: () => import('@/pages/demo2/fun3')
        }, {
            path: 'func4',
            name: `${pre}func4`,
            meta: {
                ...meta,
                title: '功能4'
            },
            component: () => import('@/pages/demo2/fun4')
        }
    ]
};
